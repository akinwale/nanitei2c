#include <Wire.h>

// I2C slave address
#define I2C_ADDRESS 0x08

// Custom I2C data commands
#define CMD_DIGITAL_WRITE 0x01
#define CMD_DIGITAL_READ 0x02
#define CMD_ANALOG_WRITE 0x03

// Pin states
#define IO_PIN_STATE_INPUT 0x01
#define IO_PIN_STATE_OUTPUT 0x02

// Buffer for reading and writing data from/to I2C
unsigned char bytes[16];
unsigned char sendBytes[5];

int analogValue = 0;
int lastReadCommand = 0;
int lastReadPin = 0;
int thisPin = 0;
int ioPinStates[53]; // pseudo hashmap
const int ioPinCount = 40;
const int ioPins[] = {
  2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32,
  33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52
};

void setup() {
  // put your setup code here, to run once:
  Wire.begin(I2C_ADDRESS);
  Serial.begin(9600);

  Wire.onReceive(onDataReceived);
  Wire.onRequest(onDataRequested);
}

void loop() {

}

bool isPinValid(int pin) {
  for (int i = 0; i < ioPinCount; i++) {
    if (ioPins[i] == pin) {
      return true;
    }
  }

  return false;
}

void onDataReceived(int byteCount) {
  int i = 0;
  int idx = 0;

  while (Wire.available()) {
    bytes[idx] = Wire.read();

    if ((idx + 1) == bytes[0]) { // Length received
      int len = (int) bytes[0];
      if (len < 3) {
        Serial.print("Invalid data received.");
        return;
      }

      switch (bytes[1]) {
        case CMD_DIGITAL_WRITE:
          if (len < 4) {
            Serial.println("Incomplete digital write data.");
            return;
          }

          thisPin = (int) bytes[2];
          if (!isPinValid(thisPin)) {
            Serial.println("Invalid pin specified for digital write.");
            return;
          }

          // do digital write
          if (ioPinStates[thisPin] != IO_PIN_STATE_OUTPUT) {
            pinMode(thisPin, OUTPUT);
            ioPinStates[thisPin] = IO_PIN_STATE_OUTPUT;
          }

          digitalWrite(thisPin, bytes[3]);

          break;

        case CMD_DIGITAL_READ:
          if (len < 3) {
            Serial.println("Incomplete digital read data.");
            return;
          }

          thisPin = (int) bytes[2];
          if (!isPinValid(thisPin)) {
            Serial.println("Invalid pin specified for digital read.");
            return;
          }

          if (ioPinStates[thisPin] != IO_PIN_STATE_OUTPUT) {
            pinMode(thisPin, OUTPUT);
            ioPinStates[thisPin] = IO_PIN_STATE_OUTPUT;
          }

          lastReadCommand = CMD_DIGITAL_READ;
          lastReadPin = thisPin;

          break;

        case CMD_ANALOG_WRITE:
          if (len < 7) {
          Serial.println("Incomplete analog write data.");
          return;
          }

          thisPin = (int) bytes[2];

          if (!isPinValid(thisPin)) {
            Serial.println("Invalid pin specified for analog write.");
            return;
          }

          analogValue = 0;
          analogValue = (uint32_t) bytes[3] << 24;
          analogValue |= (uint32_t) bytes[4] << 16;
          analogValue |= (uint32_t) bytes[5] << 8;
          analogValue |= (uint32_t) bytes[6];

          if (analogValue < 0 || analogValue > 255) {
            Serial.println("Invalid value specified for analog write.");
            return;
          }

          analogWrite(thisPin, analogValue);
          break;

        default:
          Serial.println("Unrecognised command.");
          break;
      }

      idx = 0;
    }

    idx++;
  }
}

void onDataRequested() {
  switch (lastReadCommand) {
    case CMD_DIGITAL_READ:
      // Send the value over I2C
      sendBytes[0] = 3; // length
      sendBytes[1] = lastReadPin; // the pin that was read
      sendBytes[2] = digitalRead(lastReadPin); // the pin value (0/1)
      Wire.write(sendBytes, sendBytes[0]);
      break;
  }
}