/*
 * Copyright (c) 2016 Akinwale Ariwodola <akinwale@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <fcntl.h>
#include <unistd.h>
#include <linux/i2c-dev.h>

#define MAX_BYTES 16

/**
 * Open an I2C connection using the specified file descriptor.
 */
int nanite_i2c_open(const char *filename, int slave_address)
{
    int fd;

    if ((fd = open(filename, O_RDWR)) < 0)
    {
        return -1;
    }

    if (ioctl(fd, I2C_SLAVE, slave_address) < 0)
    {
        return -1;
    }

    return fd;
}

/**
 * Close the I2C connection using the specified file descriptor.
 */
int nanite_i2c_close(int fd)
{
    if (fd > 0)
    {
        close(fd);
    }

    return 0;
}

/**
 * Send up to the maximum number of bytes over I2C using the specified file descriptor.
 */
int nanite_i2c_send(int fd, const char bytes[MAX_BYTES])
{
    if (fd <= 0 || !bytes)
    {
        return -1;
    }

    int num_bytes;
    num_bytes = write(fd, bytes, bytes[0]);
    if (num_bytes != bytes[0])
    {
        return -1;
    }

    return num_bytes;
}

/**
 * Read the byte data from I2C.
 */
int nanite_i2c_read(int fd, char bytes_read[MAX_BYTES])
{
    if (fd <= 0)
    {
        return -1;
    }

    char fbyte[1];
    // Read the first byte to get the total number of bytes to read
    if (read(fd, fbyte, 1) != 1)
    {
        return -1;
    }

    if (read(fd, bytes_read, fbyte[0]) != fbyte[0])
    {
        return -1;
    }

    return 0;
}

/**
 * Return the maximum number of bytes for a buffer for I2C communication.
 */
int nanite_i2c_max_bytes()
{
    return MAX_BYTES;
}
